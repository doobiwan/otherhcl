﻿using System;
using HipChatLib.Model;
using Newtonsoft.Json;

namespace HipChatLib
{
    public class RoomHistoryResponse
    {
        public Item[] items { get; set; }
        public Links links { get; set; }
        public int maxResults { get; set; }
        public int startIndex { get; set; }
    }

    public class Item
    {
        public string color { get; set; }
        public DateTime date { get; set; }
        public string id { get; set; }
        public User[] mentions { get; set; }
        public string message { get; set; }
        public string message_format { get; set; }
        public object from { get; set; }

        public string Author //Problem in the API
        {
            get
            {
                if (from is string)
                    return from as string;

                var f = JsonConvert.DeserializeObject<User>(from.ToString());
                return f.name;

            }
        }
    }
}