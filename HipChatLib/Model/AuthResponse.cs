﻿namespace HipChatLib
{
    public class AuthResponse
    {
        public Success success { get; set; }

        public class Success
        {
            public int code { get; set; }
            public string type { get; set; }
            public string message { get; set; }
        }
    }
}