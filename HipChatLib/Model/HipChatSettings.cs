﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HipChatLib.Model
{
    public class HipChatSettings
    {
        public const string SETTINGS_KEY = @"HipChatLib.Model.HipChatSettings.User";

        public string AccessToken { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
