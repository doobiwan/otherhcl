﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HipChatLib.Model
{
    public class RoomsResponse
    {
        public Room[] items { get; set; }
        public Links links { get; set; }
        public int maxResults { get; set; }
        public int startIndex { get; set; }
    }

    public class Room
    {
        public string id { get; set; }
        public RoomUrl links { get; set; }
        public string name { get; set; }
    }

    public class RoomUrl
    {
        public string self { get; set; }
    }

}
