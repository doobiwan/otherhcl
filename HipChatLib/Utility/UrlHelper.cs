﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HipChatLib.Utility
{
    public static class UrlHelper
    {
        public static string AppendParameter(this string url, string key, string value)
        {
            return string.Format("{0}&{1}={2}", url, key, value);
        }
    }
}
